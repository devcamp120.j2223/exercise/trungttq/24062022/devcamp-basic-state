import { Component } from "react";

class CountClick extends Component{
    constructor(props){
        super(props)

        this.state = {
            count: 0
        }
    }
    clickHandlerEvent = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    render(){
        return(
            <div>
                <button onClick={this.clickHandlerEvent}>Click me!</button>
                <p>CountClick: {this.state.count} times</p>
            </div>
        )
    }
}
export default CountClick